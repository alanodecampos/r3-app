import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {IndexPage} from './index.page';
import {IndexGuard} from "../guard/index.guard";

const routes: Routes = [
    {
        path: '',
        component: IndexPage,
        canActivate: [IndexGuard],
        children: [
            {path: '', redirectTo: 'login', pathMatch: 'full'},
            {
                path: '',
                loadChildren: () =>
                    import('../pages/welcome/welcome.module').then(
                        m => m.WelcomePageModule
                    )
            },
            {
                path: 'login',
                loadChildren: () =>
                    import('../pages/login/login.module').then(m => m.LoginPageModule)
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class IndexPageRoutingModule {
}
