import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ResultadoTestePageRoutingModule } from './resultado-teste-routing.module';

import { ResultadoTestePage } from './resultado-teste.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ResultadoTestePageRoutingModule
  ],
  declarations: [ResultadoTestePage]
})
export class ResultadoTestePageModule {}
