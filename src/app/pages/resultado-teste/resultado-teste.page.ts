import { Component, OnInit } from '@angular/core';
import {TesteNavService} from "../../service/teste-nav.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-resultado-teste',
  templateUrl: './resultado-teste.page.html',
  styleUrls: ['./resultado-teste.page.scss'],
})
export class ResultadoTestePage implements OnInit {

  teste: any = {}
  loading: any;

  constructor(private testeNavService: TesteNavService,
              private router: Router) { }

  ngOnInit() {
    this.teste = this.testeNavService.teste.teste
  }

  addResultado(teste) {
    teste.codigoTeste = teste.codigo;
    teste.codigo = null;
    this.testeNavService.teste = {teste: teste};
    this.router.navigate(['/cadastro-resultado-teste']);
  }

  itemTapped(resultado) {
    resultado.descricao = this.teste.descricao;
    this.testeNavService.teste = {teste: resultado};
    this.router.navigate(['/cadastro-resultado-teste']);
  }

}
