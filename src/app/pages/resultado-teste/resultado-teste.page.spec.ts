import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ResultadoTestePage } from './resultado-teste.page';

describe('ResultadoTestePage', () => {
  let component: ResultadoTestePage;
  let fixture: ComponentFixture<ResultadoTestePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultadoTestePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ResultadoTestePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
