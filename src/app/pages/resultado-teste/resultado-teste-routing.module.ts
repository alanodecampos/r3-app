import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ResultadoTestePage } from './resultado-teste.page';

const routes: Routes = [
  {
    path: '',
    component: ResultadoTestePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResultadoTestePageRoutingModule {}
