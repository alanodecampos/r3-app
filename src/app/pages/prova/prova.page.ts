import {Component, OnInit} from '@angular/core';
import {AuthConstants} from "../../config/auth-constants";
import {finalize} from "rxjs/operators";
import {StorageService} from "../../service/storage.service";
import {LoadingController} from "@ionic/angular";
import {AuthService} from "../../service/auth.service";
import {ProvaService} from "../../service/prova.service";
import {ToastService} from "../../service/toast.service";
import {Router} from "@angular/router";
import {ProvaNavService} from "../../service/prova-nav.service";

@Component({
    selector: 'app-prova',
    templateUrl: './prova.page.html',
    styleUrls: ['./prova.page.scss'],
})
export class ProvaPage implements OnInit {

    loading: any;
    provas: any;
    recarregar: any;

    constructor(private storageService: StorageService,
                public loadingCtrl: LoadingController,
                private authService: AuthService,
                private router: Router,
                private toastService: ToastService,
                private provaService: ProvaService,
                private provaNavService: ProvaNavService) {
    }

    ngOnInit() {
        this.getProvas();
    }

    ionViewWillEnter() {
        if (this.recarregar) {
            this.getProvas();
        }
    }

    ionViewWillLeave() {
        this.recarregar = true;
    }

    addProva() {
        this.provaNavService.prova = {prova: {}};
        this.router.navigate(['/cadastro-prova']);
    }

    itemTapped(prova) {
        this.provaNavService.prova = {prova: prova};
        this.router.navigate(['/cadastro-prova']);
    }

    async getProvas(): Promise<boolean> {

        await this.presentLoading();

        return new Promise(resolve => {
            this.storageService.get(AuthConstants.AUTH).then(res => {
                if (res.access_token) {

                    this.provaService.getProvas(res.access_token)
                    .pipe(
                        finalize(async () => {
                            // Hide the loading spinner on success or error
                            await this.loading.dismiss();
                        })
                    )
                    .subscribe(
                        (res: any) => {

                            this.provas = res;

                        },
                        (error: any) => {

                            this.refreshToken(error, res);
                        }
                    );
                    resolve(false);
                } else resolve(true);
            })
            .catch(err => {
                resolve(true);
            });
        });
    }

    async presentLoading() {
        // Prepare a loading controller
        this.loading = await this.loadingCtrl.create({
            message: ''
        });
        // Present the loading controller
        await this.loading.present();
    }

    private refreshToken(error: any, res) {

        if (error.status == 401) {

            this.authService.refreshToken(res.refresh_token)
            .subscribe(
                (res: any) => {

                    if (res) {

                        this.storageService.store(AuthConstants.AUTH, res);

                        this.getProvas();

                    } else {

                        this.toastService.presentToast('Não foi possível recuperar suas informações, por favor, entre com suas credenciais novamente');

                        this.router.navigate(['/login']);

                    }

                },
                (error: any) => {

                }
            );

        }
    }

}
