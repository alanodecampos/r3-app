import {Component, OnInit} from '@angular/core';
import {AlertController, LoadingController} from "@ionic/angular";
import {Router} from "@angular/router";
import {StorageService} from "../../service/storage.service";
import {ToastService} from "../../service/toast.service";
import {AuthConstants} from "../../config/auth-constants";
import {AuthService} from "../../service/auth.service";
import {PlayerService} from "../../service/player.service";
import {finalize} from "rxjs/operators";


@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
    providers: [AuthService]
})
export class LoginPage implements OnInit {

    postData = {
        username: '',
        password: ''
    };

    loading: any;

    constructor(
        private router: Router,
        private alertCtrl: AlertController,
        private authService: AuthService,
        private storageService: StorageService,
        private toastService: ToastService,
        private playerService: PlayerService,
        public loadingCtrl: LoadingController) {
    }

    ngOnInit() {
    }

    async login() {

        if (this.validateInputs()) {

            await this.presentLoading();

            this.authService.login(this.postData)
            .pipe(
                finalize(async () => {
                    // Hide the loading spinner on success or error
                    await this.loading.dismiss();
                })
            )
            .subscribe(
                (res: any) => {

                    if (res) {

                        this.storageService.store(AuthConstants.AUTH, res);

                        this.router.navigate(['/home']);

                        this.registerOneSignal();

                    } else {

                        this.toastService.presentToast('Serviço indisponível no momento');
                    }
                },
                (error: any) => {

                    console.log(error);

                    if (error.status == 400) {
                        this.toastService.presentToast(error.error.error_description);
                    } else {
                        this.toastService.presentToast('Network Error');
                    }
                }
            );

        } else {
            this.toastService.presentToast(
                'Por favor informe o e-mail e a senha'
            );

        }
    }

    async presentLoading() {
        // Prepare a loading controller
        this.loading = await this.loadingCtrl.create({
            message: ''
        });
        // Present the loading controller
        await this.loading.present();
    }

    validateInputs() {
        console.log(this.postData);
        let username = this.postData.username.trim();
        let password = this.postData.password.trim();
        return (
            this.postData.username &&
            this.postData.password &&
            username.length > 0 &&
            password.length > 0
        );
    }

    registerOneSignal() {
        //Remove this method to stop OneSignal Debugging
        window["plugins"].OneSignal.setLogLevel({logLevel: 6, visualLevel: 0});

        var notificationOpenedCallback = function(jsonData) {
            console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
        };

        // Set your iOS Settings
        var iosSettings = {};
        iosSettings["kOSSettingsKeyAutoPrompt"] = false;
        iosSettings["kOSSettingsKeyInAppLaunchURL"] = false;

        window["plugins"].OneSignal
        .startInit("cb71fedd-3f2d-4d34-b734-f5fd09212bd7")
        .handleNotificationOpened(notificationOpenedCallback)
        .iOSSettings(iosSettings)
        .inFocusDisplaying(window["plugins"].OneSignal.OSInFocusDisplayOption.Notification)
        .endInit();

        window["plugins"].OneSignal.addSubscriptionObserver(this.handleSubscriptionObserver);

        // The promptForPushNotificationsWithUserResponse function will show the iOS push notification prompt. We recommend removing the following code and instead using an In-App Message to prompt for notification permission (See step 6)
        window["plugins"].OneSignal.promptForPushNotificationsWithUserResponse(function(accepted) {
            console.log("User accepted notifications: " + accepted);
        });
    }


    private handleSubscriptionObserver = (state: any) => {

        console.log('handleSubscriptionObserver *********************');

        if (!state.from.subscribed && state.to.subscribed) {
            // get player ID
            console.log('PLAYER ID PLUGIN', state.to.userId)

            // registrar player ID
            console.log('REGISTRANDO PLAYERID')
            this.playerService.register(state.to.userId);
            console.log('PLAYERID REGISTRADO')
        }

    };

}
