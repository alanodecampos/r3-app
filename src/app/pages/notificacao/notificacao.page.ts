import {Component, OnInit} from '@angular/core';
import {AuthConstants} from "../../config/auth-constants";
import {finalize} from "rxjs/operators";
import {StorageService} from "../../service/storage.service";
import {LoadingController} from "@ionic/angular";
import {AuthService} from "../../service/auth.service";
import {Router} from "@angular/router";
import {ToastService} from "../../service/toast.service";
import {NotificacaoService} from "../../service/notificacao.service";

@Component({
    selector: 'app-notificacao',
    templateUrl: './notificacao.page.html',
    styleUrls: ['./notificacao.page.scss'],
})
export class NotificacaoPage implements OnInit {

    notificacoes: any;
    loading: any;

    constructor(private notificacaoService: NotificacaoService,
                private storageService: StorageService,
                public loadingCtrl: LoadingController,
                private authService: AuthService,
                private router: Router,
                private toastService: ToastService) {
    }

    ngOnInit() {
        this.getNotificacoes();
    }

    doRefresh(event) {

        this.getNotificacoes();

        setTimeout(() => {
            console.log('Async operation has ended');
            event.target.complete();
        }, 600);
    }

    async getNotificacoes(): Promise<boolean> {

        await this.presentLoading();

        return new Promise(resolve => {
            this.storageService.get(AuthConstants.AUTH).then(res => {
                if (res.access_token) {

                    this.notificacaoService.getNotificacoes(res.access_token)
                    .pipe(
                        finalize(async () => {
                            // Hide the loading spinner on success or error
                            await this.loading.dismiss();
                        })
                    )
                    .subscribe(
                        (res: any) => {

                            this.notificacoes = res;

                        },
                        (error: any) => {

                            this.refreshToken(error, res);
                        }
                    );
                    resolve(false);
                } else resolve(true);
            })
            .catch(err => {
                resolve(true);
            });
        });
    }

    async presentLoading() {
        // Prepare a loading controller
        this.loading = await this.loadingCtrl.create({
            message: ''
        });
        // Present the loading controller
        await this.loading.present();
    }

    private refreshToken(error: any, res) {

        if (error.status == 401) {

            this.authService.refreshToken(res.refresh_token)
            .subscribe(
                (res: any) => {

                    if (res) {

                        this.storageService.store(AuthConstants.AUTH, res);

                        this.getNotificacoes();

                    } else {

                        this.toastService.presentToast('Não foi possível recuperar suas informações, por favor, entre com suas credenciais novamente');

                        this.router.navigate(['/login']);

                    }

                },
                (error: any) => {


                }
            );

        }
    }
}
