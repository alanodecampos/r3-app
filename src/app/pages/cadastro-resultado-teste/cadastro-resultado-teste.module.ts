import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CadastroResultadoTestePageRoutingModule } from './cadastro-resultado-teste-routing.module';

import { CadastroResultadoTestePage } from './cadastro-resultado-teste.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CadastroResultadoTestePageRoutingModule
  ],
  declarations: [CadastroResultadoTestePage]
})
export class CadastroResultadoTestePageModule {}
