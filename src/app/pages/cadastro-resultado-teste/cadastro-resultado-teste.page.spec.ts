import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CadastroResultadoTestePage } from './cadastro-resultado-teste.page';

describe('CadastroResultadoTestePage', () => {
  let component: CadastroResultadoTestePage;
  let fixture: ComponentFixture<CadastroResultadoTestePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastroResultadoTestePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CadastroResultadoTestePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
