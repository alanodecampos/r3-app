import {Component, OnInit} from '@angular/core';
import {TesteNavService} from "../../service/teste-nav.service";
import {AuthConstants} from "../../config/auth-constants";
import {finalize} from "rxjs/operators";
import {StorageService} from "../../service/storage.service";
import {TesteService} from "../../service/teste.service";
import {LoadingController} from "@ionic/angular";
import {ToastService} from "../../service/toast.service";
import {Router} from "@angular/router";

@Component({
    selector: 'app-cadastro-resultado-teste',
    templateUrl: './cadastro-resultado-teste.page.html',
    styleUrls: ['./cadastro-resultado-teste.page.scss'],
})
export class CadastroResultadoTestePage implements OnInit {

    teste: any;
    resultado: any;
    loading: any;

    constructor(private storageService: StorageService,
                private testeService: TesteService,
                public loadingCtrl: LoadingController,
                private toastService: ToastService,
                private testeNavService: TesteNavService,
                private router: Router) {
    }

    ngOnInit() {
        this.teste = this.testeNavService.teste.teste;
    }

    async deleteResultado(codigo): Promise<boolean> {

        if (codigo) {

            await this.presentLoading();

            return new Promise(resolve => {

                this.storageService.get(AuthConstants.AUTH).then(res => {

                    if (res.access_token) {

                        this.testeService.deleteResultado(codigo, res.access_token)

                        .pipe(
                            finalize(async () => {
                                // Hide the loading spinner on success or error
                                await this.loading.dismiss();
                            })
                        )
                        .subscribe(
                            (res: any) => {

                                this.router.navigate(['/teste']);
                                this.toastService.presentToast('Resultado do teste excluído com sucesso');

                            },
                            (error: any) => {

                                this.toastService.presentToast('Houve um erro ao tentar excluir o resultado do teste');
                            }
                        );
                        resolve(false);
                    } else resolve(true);
                })
                .catch(err => {
                    resolve(true);
                });
            });

        }

    }

    async addResultado(): Promise<boolean> {

        await this.presentLoading();

        return new Promise(resolve => {

            this.storageService.get(AuthConstants.AUTH).then(res => {

                if (res.access_token) {

                    this.testeService.addResultadoTeste(this.teste, res.access_token)

                    .pipe(
                        finalize(async () => {
                            // Hide the loading spinner on success or error
                            await this.loading.dismiss();
                        })
                    )
                    .subscribe(
                        (res: any) => {

                            this.router.navigate(['/teste']);
                            this.toastService.presentToast('Resultado do teste salvo com sucesso');

                        },
                        (error: any) => {

                            this.toastService.presentToast('Houve um erro ao tentar salvar o resultado do teste');
                        }
                    );
                    resolve(false);
                } else resolve(true);
            })
            .catch(err => {
                resolve(true);
            });
        });
    }

    async presentLoading() {
        // Prepare a loading controller
        this.loading = await this.loadingCtrl.create({
            message: ''
        });
        // Present the loading controller
        await this.loading.present();
    }

}
