import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CadastroResultadoTestePage } from './cadastro-resultado-teste.page';

const routes: Routes = [
  {
    path: '',
    component: CadastroResultadoTestePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CadastroResultadoTestePageRoutingModule {}
