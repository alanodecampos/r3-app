import {Component, OnInit} from '@angular/core';
import {FeedbackNavService} from "../../service/feedback-nav.service";
import {AuthConstants} from "../../config/auth-constants";
import {finalize} from "rxjs/operators";
import {ToastService} from "../../service/toast.service";
import {StorageService} from "../../service/storage.service";
import {TreinoService} from "../../service/treino.service";
import {LoadingController} from "@ionic/angular";

@Component({
    selector: 'app-feedback-treino',
    templateUrl: './feedback-treino.page.html',
    styleUrls: ['./feedback-treino.page.scss'],
})
export class FeedbackTreinoPage implements OnInit {

    treino: any;
    loading: any;

    constructor(private treinoService: TreinoService,
                public loadingCtrl: LoadingController,
                private feedbackNavService: FeedbackNavService,
                private toastService: ToastService,
                private storageService: StorageService) {
    }

    ngOnInit() {

        this.treino = this.feedbackNavService.treino.treino;

    }

    async enviaFeedBack(): Promise<boolean> {

        await this.presentLoading();

        return new Promise(resolve => {

            this.storageService.get(AuthConstants.AUTH).then(res => {

                if (res.access_token) {

                    this.treinoService.enviaFeedback(this.treino, res.access_token)

                    .pipe(
                        finalize(async () => {
                            // Hide the loading spinner on success or error
                            await this.loading.dismiss();
                        })
                    )
                    .subscribe(
                        (res: any) => {

                            this.toastService.presentToast('Feedback enviado');

                        },
                        (error: any) => {

                            this.toastService.presentToast('Houve um erro ao tentar enviar o feedback');
                        }
                    );
                    resolve(false);
                } else resolve(true);
            })
            .catch(err => {
                resolve(true);
            });
        });
    }

    async presentLoading() {
        // Prepare a loading controller
        this.loading = await this.loadingCtrl.create({
            message: ''
        });
        // Present the loading controller
        await this.loading.present();
    }

}
