import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FeedbackTreinoPageRoutingModule } from './feedback-treino-routing.module';

import { FeedbackTreinoPage } from './feedback-treino.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FeedbackTreinoPageRoutingModule
  ],
  declarations: [FeedbackTreinoPage]
})
export class FeedbackTreinoPageModule {}
