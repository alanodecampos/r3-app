import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FeedbackTreinoPage } from './feedback-treino.page';

const routes: Routes = [
  {
    path: '',
    component: FeedbackTreinoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FeedbackTreinoPageRoutingModule {}
