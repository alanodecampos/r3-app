import { Component, OnInit } from '@angular/core';
import {BoletoNavService} from "../../service/boleto-nav.service";
import {finalize} from "rxjs/operators";
import {AuthConstants} from "../../config/auth-constants";
import {ToastService} from "../../service/toast.service";
import {LoadingController, Platform} from "@ionic/angular";
import {FinanceiroService} from "../../service/financeiro.service";
import {StorageService} from "../../service/storage.service";
import { Clipboard } from '@ionic-native/clipboard/ngx';

@Component({
  selector: 'app-boleto',
  templateUrl: './boleto.page.html',
  styleUrls: ['./boleto.page.scss'],
})
export class BoletoPage implements OnInit {

  boleto: any;
  loading: any;

  constructor(private platform: Platform,
              private boletoNavService: BoletoNavService,
              private toastService: ToastService,
              public loadingCtrl: LoadingController,
              private storageService: StorageService,
              private financeiroService: FinanceiroService,
              private clipboard: Clipboard) {

  }

  ngOnInit() {

    this.boleto = this.boletoNavService.boleto.boleto;

  }

  copiaCodigo() {
    this.clipboard.copy(this.boleto.linhaDigitavel);
  }

  async enviaBoletoPorEmail(): Promise<boolean> {

    await this.presentLoading();

    return new Promise(resolve => {
      this.storageService.get(AuthConstants.AUTH).then(res => {
        if (res.access_token) {

          this.financeiroService.enviaPorEmail(res.access_token, this.boleto.codigo)
          .pipe(
              finalize(async () => {
                // Hide the loading spinner on success or error
                await this.loading.dismiss();
              })
          )
          .subscribe(
              (res: any) => {

                this.toastService.presentToast('E-mail enviado');

              },
              (error: any) => {
                console.log('Erro ao enviar boleto por email' + error);
              }
          );
          resolve(false);
        } else resolve(true);
      })
      .catch(err => {
        resolve(true);
      });
    });
  }

  async presentLoading() {
    // Prepare a loading controller
    this.loading = await this.loadingCtrl.create({
      message: ''
    });
    // Present the loading controller
    await this.loading.present();
  }

}
