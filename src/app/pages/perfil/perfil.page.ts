import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../service/auth.service";

@Component({
  selector: 'app-perfil',
  templateUrl: 'perfil.page.html',
  styleUrls: ['perfil.page.scss']
})
export class PerfilPage implements OnInit{

  public authUser: any;

  constructor(private auth: AuthService) {
  }

  ngOnInit(): void {
    this.auth.userData$.subscribe((res: any) => {
      this.authUser = res;
    });
  }

  logoutAction() {
    this.auth.logout();
  }

}
