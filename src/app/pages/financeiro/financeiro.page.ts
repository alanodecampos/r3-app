import { Component, OnInit } from '@angular/core';
import {StorageService} from "../../service/storage.service";
import {LoadingController} from "@ionic/angular";
import {AuthService} from "../../service/auth.service";
import {Router} from "@angular/router";
import {ToastService} from "../../service/toast.service";
import {FinanceiroService} from "../../service/financeiro.service";
import {AuthConstants} from "../../config/auth-constants";
import {finalize} from "rxjs/operators";
import {BoletoNavService} from "../../service/boleto-nav.service";

@Component({
  selector: 'app-financeiro',
  templateUrl: './financeiro.page.html',
  styleUrls: ['./financeiro.page.scss'],
})
export class FinanceiroPage implements OnInit {

  debitos: any;
  loading: any;

  constructor(private financeiroService: FinanceiroService,
              private storageService: StorageService,
              public loadingCtrl: LoadingController,
              private authService: AuthService,
              private router: Router,
              private toastService: ToastService,
              private boletoNavService: BoletoNavService) { }

  ngOnInit() {
    this.getDebitos();
  }

  itemTapped( boleto) {
    this.boletoNavService.boleto = {boleto:boleto};
    this.router.navigate(['/boleto', boleto]);
  }

  async getDebitos(): Promise<boolean> {

    await this.presentLoading();

    return new Promise(resolve => {
      this.storageService.get(AuthConstants.AUTH).then(res => {
        if (res.access_token) {

          this.financeiroService.getDebitos(res.access_token)
          .pipe(
              finalize(async () => {
                // Hide the loading spinner on success or error
                await this.loading.dismiss();
              })
          )
          .subscribe(
              (res: any) => {

                this.debitos = res;

              },
              (error: any) => {

                this.refreshToken(error, res);
              }
          );
          resolve(false);
        } else resolve(true);
      })
      .catch(err => {
        resolve(true);
      });
    });
  }

  async presentLoading() {
    // Prepare a loading controller
    this.loading = await this.loadingCtrl.create({
      message: ''
    });
    // Present the loading controller
    await this.loading.present();
  }

  private refreshToken(error: any, res) {

    if (error.status == 401) {

      this.authService.refreshToken(res.refresh_token)
      .subscribe(
          (res: any) => {

            if (res) {

              this.storageService.store(AuthConstants.AUTH, res);

              this.getDebitos();

            } else {

              this.toastService.presentToast('Não foi possível recuperar suas informações, por favor, entre com suas credenciais novamente');

              this.router.navigate(['/login']);

            }

          },
          (error: any) => {

          }
      );

    }
  }
}
