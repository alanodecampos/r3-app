import {Component, OnInit, ViewChild} from '@angular/core';

import {IonSlides, LoadingController} from "@ionic/angular";
import {Router} from "@angular/router";
import {TreinoService} from "../../service/treino.service";
import {StorageService} from "../../service/storage.service";
import {AuthService} from "../../service/auth.service";
import {ToastService} from "../../service/toast.service";
import {AuthConstants} from "../../config/auth-constants";
import {finalize} from "rxjs/operators";
import {FeedbackNavService} from "../../service/feedback-nav.service";

@Component({
    selector: 'app-treino',
    templateUrl: './treino.page.html',
    styleUrls: ['./treino.page.scss'],
})

export class TreinoPage implements OnInit {

    @ViewChild('slider', {static: true}) slides: IonSlides;
    planilha: any;
    treinos: any;
    loading: any;

    constructor(private treinoService: TreinoService,
                private storageService: StorageService,
                public loadingCtrl: LoadingController,
                private authService: AuthService,
                private router: Router,
                private toastService: ToastService,
                private feedbackNavService: FeedbackNavService) {
    }

    ngOnInit(): void {

        this.getTreino();

    }

    itemTapped(treino) {

        this.feedbackNavService.treino = {treino: treino};
        this.router.navigate(['/feedback-treino', treino]);
    }

    doRefresh(event) {

        this.getTreino();

        setTimeout(() => {
            console.log('Async operation has ended');
            event.target.complete();
        }, 600);
    }

    async getTreino(): Promise<boolean> {

        await this.presentLoading();

        return new Promise(resolve => {
            this.storageService.get(AuthConstants.AUTH).then(res => {
                if (res.access_token) {

                    this.treinoService.getTreino(res.access_token)
                    .pipe(
                        finalize(async () => {
                            // Hide the loading spinner on success or error
                            await this.loading.dismiss();
                        })
                    )
                    .subscribe(
                        (res: any) => {

                            this.planilha = res;
                            this.treinos = this.planilha.treinos;

                            this.ionViewDidEnter();

                        },
                        (error: any) => {

                            this.refreshToken(error, res);
                        }
                    );
                    resolve(false);
                } else resolve(true);
            })
            .catch(err => {
                resolve(true);
            });
        });
    }

    async presentLoading() {
        // Prepare a loading controller
        this.loading = await this.loadingCtrl.create({
            message: ''
        });
        // Present the loading controller
        await this.loading.present();
    }

    async enviaFeedBack(treino): Promise<boolean> {

        await this.presentLoading();

        return new Promise(resolve => {

            this.storageService.get(AuthConstants.AUTH).then(res => {

                if (res.access_token) {

                    this.treinoService.enviaFeedback(treino, res.access_token)

                    .pipe(
                        finalize(async () => {
                            // Hide the loading spinner on success or error
                            await this.loading.dismiss();
                        })
                    )
                    .subscribe(
                        (res: any) => {

                            console.log('FEEDBACK ENVIADO!!!');
                            this.toastService.presentToast('Feedback enviado');

                        },
                        (error: any) => {

                            this.refreshToken(error, res);
                        }
                    );
                    resolve(false);
                } else resolve(true);
            })
            .catch(err => {
                resolve(true);
            });
        });
    }

    private ionViewDidEnter(): void {
        if (this.planilha) {
            this.slides.getSwiper().then(sw => sw.init()).then(() => {
                this.slides.slideTo(this.planilha.indexDay, 400);
            });
        }
    }

    private refreshToken(error: any, res) {

        if (error.status == 401) {

            this.authService.refreshToken(res.refresh_token)
            .subscribe(
                (res: any) => {

                    if (res) {

                        this.storageService.store(AuthConstants.AUTH, res);

                        this.getTreino();

                    } else {

                        this.toastService.presentToast('Não foi possível recuperar suas informações, por favor, entre com suas credenciais novamente');

                        this.router.navigate(['/login']);

                    }

                },
                (error: any) => {


                }
            );

        }
    }

}
