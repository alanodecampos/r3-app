import {Component, OnDestroy, OnInit} from '@angular/core';
import {StorageService} from "../../service/storage.service";
import {LoadingController, NavController} from "@ionic/angular";
import {AuthService} from "../../service/auth.service";
import {Router} from "@angular/router";
import {ToastService} from "../../service/toast.service";
import {TesteService} from "../../service/teste.service";
import {AuthConstants} from "../../config/auth-constants";
import {finalize} from "rxjs/operators";
import {TesteNavService} from "../../service/teste-nav.service";

@Component({
    selector: 'app-teste',
    templateUrl: './teste.page.html',
    styleUrls: ['./teste.page.scss'],
})
export class TestePage implements OnInit {

    loading: any;
    testes: any;
    recarregar: any;

    constructor(private storageService: StorageService,
                public loadingCtrl: LoadingController,
                private authService: AuthService,
                private router: Router,
                private toastService: ToastService,
                private testeService: TesteService,
                private testeNavService: TesteNavService) {
    }

    ngOnInit() {
        this.getTestes();
    }

    ionViewWillEnter() {
        if (this.recarregar) {
            this.getTestes();
        }
    }

    ionViewWillLeave() {
        this.recarregar = true;
    }


    itemTapped(teste) {
        this.testeNavService.teste = {teste: teste};
        this.router.navigate(['/resultado-teste']);
    }

    async getTestes(): Promise<boolean> {

        await this.presentLoading();

        return new Promise(resolve => {
            this.storageService.get(AuthConstants.AUTH).then(res => {
                if (res.access_token) {

                    this.testeService.getTestes(res.access_token)
                    .pipe(
                        finalize(async () => {
                            // Hide the loading spinner on success or error
                            await this.loading.dismiss();
                        })
                    )
                    .subscribe(
                        (res: any) => {

                            this.testes = res;

                        },
                        (error: any) => {

                            this.refreshToken(error, res);
                        }
                    );
                    resolve(false);
                } else resolve(true);
            })
            .catch(err => {
                resolve(true);
            });
        });
    }

    async presentLoading() {
        // Prepare a loading controller
        this.loading = await this.loadingCtrl.create({
            message: ''
        });
        // Present the loading controller
        await this.loading.present();
    }

    private refreshToken(error: any, res) {

        if (error.status == 401) {

            this.authService.refreshToken(res.refresh_token)
            .subscribe(
                (res: any) => {

                    if (res) {

                        this.storageService.store(AuthConstants.AUTH, res);

                        this.getTestes();

                    } else {

                        this.toastService.presentToast('Não foi possível recuperar suas informações, por favor, entre com suas credenciais novamente');

                        this.router.navigate(['/login']);

                    }

                },
                (error: any) => {

                }
            );

        }
    }

}
