import {Component, OnInit} from '@angular/core';
import {ProvaService} from "../../service/prova.service";
import {AuthConstants} from "../../config/auth-constants";
import {finalize} from "rxjs/operators";
import {LoadingController} from "@ionic/angular";
import {ToastService} from "../../service/toast.service";
import {StorageService} from "../../service/storage.service";
import {ProvaNavService} from "../../service/prova-nav.service";
import {Router} from "@angular/router";

@Component({
    selector: 'app-cadastro-prova',
    templateUrl: './cadastro-prova.page.html',
    styleUrls: ['./cadastro-prova.page.scss'],
})
export class CadastroProvaPage implements OnInit {

    prova: any = {}
    loading: any;

    constructor(private provaService: ProvaService,
                public loadingCtrl: LoadingController,
                private toastService: ToastService,
                private storageService: StorageService,
                private provaNavService: ProvaNavService,
                private router: Router) {
    }

    ngOnInit() {
        this.prova = this.provaNavService.prova.prova;
    }

    async deleteProva(codigo): Promise<boolean> {

        if (codigo) {

            await this.presentLoading();

            return new Promise(resolve => {

                this.storageService.get(AuthConstants.AUTH).then(res => {

                    if (res.access_token) {

                        this.provaService.deleteProva(codigo, res.access_token)

                        .pipe(
                            finalize(async () => {
                                // Hide the loading spinner on success or error
                                await this.loading.dismiss();
                            })
                        )
                        .subscribe(
                            (res: any) => {

                                this.router.navigate(['/prova']);
                                this.toastService.presentToast('Prova excluída com sucesso');

                            },
                            (error: any) => {

                                this.toastService.presentToast('Houve um erro ao tentar excluir a Prova');
                            }
                        );
                        resolve(false);
                    } else resolve(true);
                })
                .catch(err => {
                    resolve(true);
                });
            });

        }

    }

    async addProva(): Promise<boolean> {

        await this.presentLoading();

        return new Promise(resolve => {

            this.storageService.get(AuthConstants.AUTH).then(res => {

                if (res.access_token) {

                    this.provaService.addProva(this.prova, res.access_token)

                    .pipe(
                        finalize(async () => {
                            // Hide the loading spinner on success or error
                            await this.loading.dismiss();
                        })
                    )
                    .subscribe(
                        (res: any) => {

                            this.router.navigate(['/prova']);
                            this.toastService.presentToast('Prova salva com sucesso');

                        },
                        (error: any) => {

                            this.toastService.presentToast('Houve um erro ao tentar salvar a Prova');
                        }
                    );
                    resolve(false);
                } else resolve(true);
            })
            .catch(err => {
                resolve(true);
            });
        });
    }

    async presentLoading() {
        // Prepare a loading controller
        this.loading = await this.loadingCtrl.create({
            message: ''
        });
        // Present the loading controller
        await this.loading.present();
    }

}
