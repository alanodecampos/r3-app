import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CadastroProvaPage } from './cadastro-prova.page';

const routes: Routes = [
  {
    path: '',
    component: CadastroProvaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CadastroProvaPageRoutingModule {}
