import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CadastroProvaPageRoutingModule } from './cadastro-prova-routing.module';

import { CadastroProvaPage } from './cadastro-prova.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CadastroProvaPageRoutingModule
  ],
  declarations: [CadastroProvaPage]
})
export class CadastroProvaPageModule {}
