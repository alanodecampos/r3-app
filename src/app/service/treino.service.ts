import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {StorageService} from "./storage.service";

@Injectable({
    providedIn: 'root'
})
export class TreinoService {

    constructor(private http: HttpClient,
                private storageService: StorageService) {
    }

    getTreino(access_token) {

        const url = environment.apiUrl + '/planilha';

        // Headers
        let httpOptions = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + access_token
            })
        };

        return this.http.get(url, httpOptions);
    }

    enviaFeedback(treino, access_token) {

        const url = environment.apiUrl + '/planilha/feedback';

        let body = {
            'treinoId': treino.codigo,
            'realizado': treino.realizado,
            'local': treino.local,
            'tempo': treino.tempo,
            'pace': treino.pace,
            'feedback': treino.feedback
        };

        // Headers
        let httpOptions = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + access_token
            })
        };

        return this.http.put(url, body, httpOptions);
    }
}
