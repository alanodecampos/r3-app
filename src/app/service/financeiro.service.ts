import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable({
    providedIn: 'root'
})
export class FinanceiroService {

    constructor(private http: HttpClient) {
    }

    getDebitos(access_token) {

        const url = environment.apiUrl + '/debitos';

        // Headers
        let httpOptions = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + access_token
            })
        };

        return this.http.get(url, httpOptions);
    }

    enviaPorEmail(access_token, parcelaId) {

        const url = environment.apiUrl + '/debitos/envia/email/' + parcelaId;

        // Headers
        let httpOptions = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + access_token
            })
        };

        return this.http.get(url, httpOptions);
    }
}
