import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable({
    providedIn: 'root'
})
export class ProvaService {

    constructor(private http: HttpClient) {
    }

    getProvas(access_token) {

        const url = environment.apiUrl + '/provas';

        // Headers
        let httpOptions = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + access_token
            })
        };

        return this.http.get(url, httpOptions);
    }

    deleteProva(codigo, access_token) {

        const url = environment.apiUrl + '/provas/' + codigo;

        // Headers
        let httpOptions = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + access_token
            })
        };

        return this.http.delete(url, httpOptions);
    }

    addProva(prova, access_token) {

        const url = environment.apiUrl + '/provas';

        let body = {
            'codigo': prova.codigo,
            'descricao': prova.descricao,
            'distancia': prova.distancia,
            'data': prova.data,
            'provaAlvo': prova.provaAlvo
        };

        // Headers
        let httpOptions = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + access_token
            })
        };

        return this.http.post(url, body, httpOptions);
    }

}
