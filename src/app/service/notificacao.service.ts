import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable({
    providedIn: 'root'
})
export class NotificacaoService {

    constructor(private http: HttpClient) {
    }

    getNotificacoes(access_token) {

        const url = environment.apiUrl + '/notificacoes';

        // Headers
        let httpOptions = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + access_token
            })
        };

        return this.http.get(url, httpOptions);
    }
}
