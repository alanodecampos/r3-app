import { TestBed } from '@angular/core/testing';

import { FeedbackNavService } from './feedback-nav.service';

describe('FeedbackNavService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FeedbackNavService = TestBed.get(FeedbackNavService);
    expect(service).toBeTruthy();
  });
});
