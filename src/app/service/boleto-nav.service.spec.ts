import { TestBed } from '@angular/core/testing';

import { BoletoNavService } from './boleto-nav.service';

describe('BoletoNavService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BoletoNavService = TestBed.get(BoletoNavService);
    expect(service).toBeTruthy();
  });
});
