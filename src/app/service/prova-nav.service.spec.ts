import { TestBed } from '@angular/core/testing';

import { ProvaNavService } from './prova-nav.service';

describe('ProvaNavService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProvaNavService = TestBed.get(ProvaNavService);
    expect(service).toBeTruthy();
  });
});
