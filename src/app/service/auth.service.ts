import { Injectable } from '@angular/core';
import {StorageService} from "./storage.service";
import {AuthConstants} from "../config/auth-constants";
import {Router} from "@angular/router";
import {BehaviorSubject, Observable} from "rxjs";
import {HttpService} from "./http.service";
import {environment} from "../../environments/environment";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  userData$ = new BehaviorSubject<any>([]);

  constructor(private httpService: HttpService,
              private http: HttpClient,
              private storageService: StorageService,
              private router: Router) { }

  getUserData() {
    this.storageService.get(AuthConstants.AUTH).then(res => {
      this.userData$.next(res);
    });
  }

  login(postData: any): Observable<any> {

    const url = environment.authUrl + '/oauth/token';

    // Headers
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Basic Y29lbGhvLWF1dGg6Y29lbGhvYXV0aDEzMjI5'
      })
    };

    const body = new HttpParams()
    .set('username', postData.username)
    .set('password', postData.password)
    .set('grant_type', 'password');

    return this.http.post(url, body.toString(), httpOptions);

  }

  refreshToken(refresh_token: any): Observable<any> {

    const url = environment.authUrl + '/oauth/token';

    // Headers
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Basic Y29lbGhvLWF1dGg6Y29lbGhvYXV0aDEzMjI5'
      })
    };

    const body = new HttpParams()
    .set('refresh_token', refresh_token)
    .set('grant_type', 'refresh_token');

    return this.http.post(url, body.toString(), httpOptions);

  }

  logout() {
    this.storageService.removeStorageItem(AuthConstants.AUTH).then(res => {
      this.userData$.next('');
      this.router.navigate(['/login']);
    });
  }
}
