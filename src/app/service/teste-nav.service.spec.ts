import { TestBed } from '@angular/core/testing';

import { TesteNavService } from './teste-nav.service';

describe('TesteNavService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TesteNavService = TestBed.get(TesteNavService);
    expect(service).toBeTruthy();
  });
});
