import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class TesteService {

  constructor(private http: HttpClient) {
  }

  getTestes(access_token) {

    const url = environment.apiUrl + '/testes';

    // Headers
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + access_token
      })
    };

    return this.http.get(url, httpOptions);
  }

  deleteResultado(codigo, access_token) {

    const url = environment.apiUrl + '/testes/' + codigo;

    // Headers
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + access_token
      })
    };

    return this.http.delete(url, httpOptions);
  }

  addResultadoTeste(resultado, access_token) {

    const url = environment.apiUrl + '/testes';

    let body = {
      'codigo': resultado.codigo,
      'codigoTeste': resultado.codigoTeste,
      'data': resultado.data,
      'tempo': resultado.tempo
    };

    // Headers
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + access_token
      })
    };

    return this.http.post(url, body, httpOptions);
  }
}
