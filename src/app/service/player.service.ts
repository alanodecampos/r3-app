import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {AuthConstants} from "../config/auth-constants";
import {StorageService} from "./storage.service";

@Injectable({
    providedIn: 'root'
})
export class PlayerService {

    constructor(private http: HttpClient,
                private storageService: StorageService) {
    }

    register(playerId): Promise<boolean> {

        return new Promise(resolve => {
            this.storageService.get(AuthConstants.AUTH).then(res => {
                if (res.access_token) {
                    this.send(playerId, res.access_token).subscribe(
                        (res: any) => {

                            console.log('PLAYER REGISTRADO COM SUCESSO', res)

                        },
                        (error: any) => {

                            console.log('ERRO AO REGISTRAR PLAYER', error)
                        }
                    );
                    resolve(false);
                } else resolve(true);
            })
            .catch(err => {
                resolve(true);
            });
        });

    }

    send(playerId, access_token) {

        const url = environment.apiUrl + '/players';

        let body = {'playerId': playerId};

        // Headers
        let httpOptions = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + access_token
            })
        };

        return this.http.post(url, body, httpOptions);
    }
}
