import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
        import('./index/index.module').then(m => m.IndexPageModule)
  },
  {
    path: '',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'financeiro',
    loadChildren: () =>
        import('./pages/financeiro/financeiro.module').then(
            m => m.FinanceiroPageModule)
  },
  {
    path: 'boleto',
    loadChildren: () => import('./pages/boleto/boleto.module').then( m => m.BoletoPageModule)
  },
  {
    path: 'feedback-treino',
    loadChildren: () => import('./pages/feedback-treino/feedback-treino.module').then( m => m.FeedbackTreinoPageModule)
  },
  {
    path: 'prova',
    loadChildren: () => import('./pages/prova/prova.module').then( m => m.ProvaPageModule)
  },
  {
    path: 'cadastro-prova',
    loadChildren: () => import('./pages/cadastro-prova/cadastro-prova.module').then( m => m.CadastroProvaPageModule)
  },
  {
    path: 'teste',
    loadChildren: () => import('./pages/teste/teste.module').then( m => m.TestePageModule)
  },
  {
    path: 'resultado-teste',
    loadChildren: () => import('./pages/resultado-teste/resultado-teste.module').then( m => m.ResultadoTestePageModule)
  },
  {
    path: 'cadastro-resultado-teste',
    loadChildren: () => import('./pages/cadastro-resultado-teste/cadastro-resultado-teste.module').then( m => m.CadastroResultadoTestePageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
