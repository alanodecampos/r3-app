import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {HomePage} from './home.page';
import {HomeGuard} from "../guard/home.guard";
import {UserDataResolver} from "../resolver/user-data.resolver";

const routes: Routes = [
    {
        path: 'home',
        component: HomePage,
        canActivate: [HomeGuard],
        resolve: {
            userData: UserDataResolver
        },
        children: [
            {
                path: 'feed',
                loadChildren: () =>
                    import('../pages/feed/feed.module').then(m => m.FeedPageModule)
            },
            {
                path: 'treino',
                loadChildren: () =>
                    import('../pages/treino/treino.module').then(
                        m => m.TreinoPageModule
                    )
            },
            {
                path: 'notificacao',
                loadChildren: () =>
                    import('../pages/notificacao/notificacao.module').then(
                        m => m.NotificacaoPageModule
                    )
            },
            {
                path: 'perfil',
                loadChildren: () =>
                    import('../pages/perfil/perfil.module').then(
                        m => m.PerfilPageModule
                    )
            },
            {
                path: 'financeiro',
                loadChildren: () =>
                    import('../pages/financeiro/financeiro.module').then(
                        m => m.FinanceiroPageModule)
            },
            {
                path: '',
                redirectTo: '/home/feed',
                pathMatch: 'full'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class HomePageRoutingModule {
}
